all: bios.bin

real.o: Makefile real.s
	as real.s --32 -o real.o

unreal.o: Makefile unreal.s
	as unreal.s --32 -o unreal.o

bios.elf: Makefile link.ld real.o
	ld -T link.ld real.o -o bios.elf

bios.bin: Makefile bios.elf
	objcopy --remove-section .note.gnu.property -O binary --pad-to 0x100000 bios.elf bios.bin
